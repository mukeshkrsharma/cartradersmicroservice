package edu.ait.cartraders.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cars")
public class Car {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String make;
	private String model;
	private int year;
	@Column(name="color")
	private String colour;
	private double litre;
	private int mileage;
	private double price;
	@Enumerated(EnumType.STRING)
	private Condition condition;
	private String seller;

	public Car() {

	}

	public Car(Integer id, String make, String model, int year, String colour, double litre, int mileage, double price,
			String seller) {
		super();
		this.id = id;
		this.make = make;
		this.model = model;
		this.year = year;
		this.colour = colour;
		this.litre = litre;
		this.mileage = mileage;
		this.price = price;
		this.seller = seller;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMake() {
		return make;
	}

	public void setMake(final String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(final String model) {
		this.model = model;
	}

	public int getYear() {
		return year;
	}

	public void setYear(final int year) {
		this.year = year;
	}

	public String getColor() {
		return colour;
	}

	public void setColor(final String color) {
		this.colour = color;
	}

	public double getLitre() {
		return litre;
	}

	public void setLitre(final double litre) {
		this.litre = litre;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(final int mileage) {
		this.mileage = mileage;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(final double price) {
		this.price = price;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(final Condition condition) {
		this.condition = condition;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(final String seller) {
		this.seller = seller;
	}

	@Override
	public String toString() {
		return "Car [id=" + id + ", make=" + make + ", model=" + model + ", year=" + year + ", color=" + colour
				+ ", litre=" + litre + ", mileage=" + mileage + ", price=" + price + ", seller=" + seller + "]";
	}

}

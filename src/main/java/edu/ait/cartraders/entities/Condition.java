package edu.ait.cartraders.entities;

public enum Condition {
	USED, NEW;
}

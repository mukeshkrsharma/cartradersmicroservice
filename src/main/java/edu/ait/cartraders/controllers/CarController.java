package edu.ait.cartraders.controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import edu.ait.cartraders.entities.Car;
import edu.ait.cartraders.exception.CarNotFoundException;
import edu.ait.cartraders.repositories.CarRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("api/v1")
@Api(value = "Assesment", description = "Car Trader  API")
public class CarController {

	@Autowired
	CarRepository carRespository;

	/**
	 * 
	 * @return
	 */
	@GetMapping("cars")
	@ApiOperation(value = "Get all cars")
	public List<Car> getAllCars() {
		return carRespository.findAll();
	}

	/**
	 * 
	 * @param carId
	 * @return
	 */
	@GetMapping("cars/{carId}")
	@ApiOperation(value = "Get car by Id")
	public Optional<Car> getCarById(final @PathVariable int carId) {
		return carRespository.findById(carId);
	}

	/**
	 * 
	 * @param carId
	 * @return
	 */
	// Adding this method because in the requirement document you mentioned each
	// step require separate REST endpoint , however this method serves both purpose
	// of find car by id if exist and not exist.
	@ApiOperation(value = "Get car by Id which doesn't exist")
	@GetMapping("cars/carnotfound/{carId}")
	public Optional<Car> getCarByIdDoesNotExist(final @PathVariable int carId) {
		if ((carRespository.findById(carId).isPresent())) {
			return carRespository.findById(carId);
		} else {
			throw new CarNotFoundException("Unable to find car with ID :" + carId);
		}
	}

	/**
	 * 
	 * @param carId
	 */
	@DeleteMapping("cars/{carId}")
	@ApiOperation(value = "Delete car by Id")
	public void deleteCarById(@PathVariable int carId) {
		try {
			carRespository.deleteById(carId);
		} catch (EmptyResultDataAccessException e) {
			throw new CarNotFoundException("Unable to delete car with ID :" + carId);
		}

	}

	/**
	 * 
	 * @param car
	 * @return
	 */
	@PostMapping("cars/")
	@ApiOperation(value = "Create car")
	public ResponseEntity<?> createCar(@RequestBody Car car) {
		carRespository.save(car);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(car.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	/**
	 * 
	 * @param car
	 * @return
	 */
	@PutMapping("cars/")
	@ApiOperation(value = "update car")
	public ResponseEntity<?> updateCar(@RequestBody Car car) {
		if (car.getId() != null) {
			carRespository.save(car);
			return ResponseEntity.status(HttpStatus.OK).build();
		} else {
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(car.getId())
					.toUri();
			return ResponseEntity.created(location).build();
		}
	}

	/**
	 * 
	 * @param year
	 * @return
	 */
	@GetMapping("cars/year/{year}")
	@ApiOperation(value = "Search car by Year")
	public List<Car> searchCarByYear(final @PathVariable int year) {
		return carRespository.findCarByYear(year);
	}

	/**
	 * 
	 * @param make
	 * @param model
	 * @return
	 */
	@GetMapping("cars/makeAndModel/{make}/{model}")
	@ApiOperation(value = "Search car by Make and Model")
	public List<Car> searchCarByMakeAndModel(final @PathVariable String make, final @PathVariable String model) {
		return carRespository.findCarByMakeAndModel(make, model);
	}

	/**
	 * 
	 * @param mileage
	 * @return
	 */
	@GetMapping("cars/mileage/{mileage}")
	@ApiOperation(value = "Search car by Mileage Less Than")
	public List<Car> searchCarByMileageLessThan(final @PathVariable int mileage) {
		return carRespository.findByMileageLessThan(mileage);
	}

}

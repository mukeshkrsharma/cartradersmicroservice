package edu.ait.cartraders.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ait.cartraders.entities.Car;

public interface CarRepository extends JpaRepository<Car, Integer> {

	List<Car> findCarByYear(final int year);

	List<Car> findCarByMakeAndModel(final String make, final String model);

	List<Car> findByMileageLessThan(final int mileage);

}
